var HtmlWebpackPlugin = require('html-webpack-plugin');
var products = [{ url: 'cuewax.html', name: 'Cue Wax' },
                { url: 'shaftcleaner.html', name: 'Shaft Cleaner' },
                { url: 'dentbuffer.html', name: 'Dent Buffer' },
                { url: 'cuecloth.html', name: 'Cue Cloth' }];

function contentPlugin(template, active) {
    return new HtmlWebpackPlugin({
            template: './src/templates/' + template,
            filename: template.split('.')[0] + '.html',
            inject: false,
            products: products,
            active: active
    });
}
module.exports = {
    entry: "./src/ts/index.tsx",
    output: {
        filename: "js/bundle.js",
        path: __dirname + "/dist"
    },

    // Enable sourcemaps for debugging webpack's output.
    devtool: "source-map",

    resolve: {
        // Add '.ts' and '.tsx' as resolvable extensions.
        extensions: [".ts", ".tsx", ".js", ".json"]
    },

    module: {
        rules: [
            // All files with a '.ts' or '.tsx' extension will be handled by 'awesome-typescript-loader'.
            { test: /\.tsx?$/, loader: "awesome-typescript-loader" },
            { test: /\.(jpg|ico|svg)$/, loader: "file-loader?name=[name].[ext]&outputPath=images/"},
            { test: /\.css$/, loader: "file-loader?name=[name].[ext]&outputPath=css/!extract-loader!css-loader"},
            { test: /\.cert$/, loader: "file-loader?name=[name].html&outputPath=.well-known/"},
            { test: /\.pug$/, loader: "pug-loader"},
            { enforce: "pre", test: /\.js$/, loader: "source-map-loader" }
        ]
    },
    plugins: [
        contentPlugin('index.pug', -1),
        contentPlugin('cuewax.pug', 0),
        contentPlugin('shaftcleaner.pug', 1),
        contentPlugin('dentbuffer.pug', 2),
        contentPlugin('cuecloth.pug', 3),
        new HtmlWebpackPlugin({
            template: './src/templates/cert.pug',
            filename: '.well-known/acme-challenge/vd_vKwNbdD3yG31pAk-PerkXtFahyOBrHyQ5kiR6DrY',
            inject: false
        })
    ],
    // When importing a module whose path matches one of the following, just
    // assume a corresponding global variable exists and use that instead.
    // This is important because it allows us to avoid bundling all of our
    // dependencies, which allows browsers to cache those libraries between builds.
    externals: {
        "react": "React",
        "react-dom": "ReactDOM",
    },
};