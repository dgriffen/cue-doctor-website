import * as React from "react";
import { CueDocNav } from './Nav';
import { Home, CueWax, ShaftCleaner, DentBuffer, CueCloth } from './Content';
import { DisplayBox } from './Retail';
import { BrowserRouter as Router, Route } from 'react-router-dom';


export const App = () =>
    <Router>
        <div>
            <CueDocNav />
            <Route exact path="/" component={Home} />
            <Route path="/cuewax.html" component={CueWax} />
            <Route path="/shaftcleaner.html" component={ShaftCleaner} />
            <Route path="/dentbuffer.html" component={DentBuffer} />
            <Route path="/cuecloth.html" component={CueCloth} />
            <Route path="/displaybox.html" component={DisplayBox} />
        </div>
    </Router>;