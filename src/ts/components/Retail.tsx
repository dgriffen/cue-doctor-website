import * as React from "react";
import { Route } from "react-router-dom";
import { Grid, Well, Row, Col, Image, Table } from 'react-bootstrap';
require('../../images/cueaid.jpg');
require('../../images/cuecare.jpg');
export const DisplayBox = () =>
    <Grid><Well>
        <Row>
            <Col md={12} componentClass={'h2'}>Cue Care Center Display Box</Col>
        </Row>
        <Row>
            <Col md={9}>
                <Row>
                    <Col md={12} componentClass={'p'}>
                        <Table responsive>
                            <tbody>
                                <tr>
                                    <td>1-20 Boxes</td>
                                    <td>$215.95 ea.</td>
                                </tr>
                                <tr>
                                    <td>21-50 Boxes</td>
                                    <td>$195.95 ea.</td>
                                </tr>
                                <tr>
                                    <td>50+ Boxes</td>
                                    <td>$175.95 ea.</td>
                                </tr>
                            </tbody>
                        </Table>
                    </Col>
                </Row>
                <Row>
                    <Col md={12} componentClass={'p'}>
                        Each box contains:
                        <ul>
                            <li>15 Cleaner & Conditioner</li>
                            <li>15 Cue Wax</li>
                            <li>15 Dent Buffer</li>
                            <li>15 MagiCloth</li>
                        </ul>
                        Retail value per box: $478.50
                    </Col>
                </Row>
            </Col>
            <Col md={3}>
                <Image responsive rounded src="/images/cuecare.jpg" />
            </Col>
        </Row>
    </Well >
        <Well>
            <Row>
                <Col md={12} componentClass={'h2'}>Cue Aid Kit</Col>
            </Row>
            <Row>
                <Col md={9}>
                    <Row>
                        <Col md={12} componentClass={'p'}>
                            $29.95 ea.
                        </Col>
                    </Row>
                    <Row>
                        <Col md={12} componentClass={'p'}>
                            Each kit contains:
                            <ul>
                                <li>1 Cleaner & Conditioner</li>
                                <li>1 Cue Wax</li>
                                <li>1 Dent Buffer</li>
                                <li>1 MagiCloth</li>
                            </ul>
                            Retail value per box: $478.50
                        </Col>
                    </Row>
                </Col>
                <Col md={3}>
                    <Image responsive rounded src="/images/cueaid.jpg" />
                </Col>
            </Row>
        </Well ></Grid>;