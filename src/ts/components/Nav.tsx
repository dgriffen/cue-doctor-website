import * as React from "react";
import { Navbar, Nav } from 'react-bootstrap';
import { Route, Link } from 'react-router-dom';

const ListItemLink = (props: { to: string, children?: any, onSelect?: any }) =>
    <Route path={props.to} children={({ match }) =>
        <li role="presentation" className={match ? 'active' : ''}>
            <Link to={props.to} onClick={props.onSelect}> {...props.children}</Link>
        </li>}
    />;

export const CueDocNav = () =>
    <Navbar inverse fixedTop collapseOnSelect>
        <Navbar.Header>
            <Navbar.Brand>
                <Link to={'/'}>
                    <img alt="Logo" src="/images/cue_doctor_logo_plain_bg.svg" />
                </Link>
            </Navbar.Brand>
            <Navbar.Toggle />
        </Navbar.Header>
        <Navbar.Collapse>
            <Nav>
                <ListItemLink to="/cuewax.html">Cue Wax</ListItemLink>
                <ListItemLink to="/shaftcleaner.html">Shaft Cleaner</ListItemLink>
                <ListItemLink to="/dentbuffer.html">Dent Buffer</ListItemLink>
                <ListItemLink to="/cuecloth.html">Cue Cloth</ListItemLink>
            </Nav>
            <Nav pullRight>
                <ListItemLink to="/displaybox.html">Retailers</ListItemLink>
            </Nav>
        </Navbar.Collapse>
    </Navbar>